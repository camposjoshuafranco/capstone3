import { useState, useEffect } from 'react'; 
import './App.css';
import { AppNavBar } from './components/AppNavBar';
import 'bootstrap/dist/css/bootstrap.min.css';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
// import { useState } from 'react';

import ScrollToTop from './components/ScrollToTop';

import { Home } from './pages/Home';
import { Arcade } from './pages/Arcade';
import { Street } from './pages/Street';
import { ProductView } from './pages/ProductView';
import { ProductAll } from './pages/ProductsAll';
import { AddProduct } from './pages/AddProduct';
import { Login } from './pages/Login';

import { UserProvider } from './UserContext';
import { Logout } from './pages/Logout';
import { Register } from './pages/Register';
import { ErrorPage } from './pages/ErrorPage';
import { ProductActive } from './pages/ProductsActive';
import { UpdateProduct } from './pages/UpdateProduct';




function App() {

  const [user,setUser] = useState({
    id:null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      isAdmin:null
    })
  }

  useEffect(() => {

    let token = localStorage.getItem('accessToken')

    fetch('https://young-plateau-70373.herokuapp.com/users/profile',{
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(convertedData => {
      
      if (typeof convertedData._id !== "undefined") {
        setUser ({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        })
      } else {
        setUser ({
          id: null,
          isAdmin: null
        })
      }
    })
  },[user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <ScrollToTop />
        <AppNavBar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/collection/arcade' element={<Arcade />} />
          <Route path='/collection/street' element={<Street />} />
          
          <Route path='/product/view/:id' element={<ProductView />}  />
         

          <Route path='/products' element={<ProductActive />} />
          <Route path='/login' element={<Login />}  />
          <Route path='/register' element={<Register />}  />
          <Route path='/logout' element={<Logout />}  />
          <Route path='*' element={<ErrorPage />} />

          {user.isAdmin ?
          <> 
          <Route path='/product/update/:id' element={<UpdateProduct />}  />
          <Route path='/products/create' element={<AddProduct />}  />
          <Route path='/products/database' element={<ProductAll />} />
          </>
           : <></>}

        </Routes>
      </Router> 
    </UserProvider>
  );
}

export default App;

