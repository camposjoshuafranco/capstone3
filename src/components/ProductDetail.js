import React from 'react'
import { Carousel } from 'react-bootstrap'

    export const ProductDetail = ({data}) => {
        return(
            <Carousel variant="light" className='w-75 mx-auto'>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={data.img1}
                    alt="First slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={data.img2}
                    alt="Second slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={data.img3}
                    alt="Third slide"
                    />
                </Carousel.Item>
            </Carousel>              
        )
    } 
    