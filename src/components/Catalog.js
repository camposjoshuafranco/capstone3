import React, { useState, useEffect } from "react";
import { Row, Col, Container } from "react-bootstrap";

import { ProductCard } from "./ProductCard";


export const Catalog = () => {
    const [productCollection, setProductCollection] = useState([])

    useEffect(() => {

        //turn res to json format
        fetch('https://young-plateau-70373.herokuapp.com/products/active')
        .then(res => res.json())
        .then(convertedData => {
            setProductCollection(convertedData.map(product => {
            return (
                <Col xs={12} md={6} lg={4} key={product._id}>
                    <ProductCard productProp={product} />
                </Col>
            )
        }))
        })

    }, [productCollection]);

    return (
        <Container className="text-center">
            <h3 className='text font-weight-bold'>PRODUCTS</h3>
            <Row>
                {productCollection[0]}
                {productCollection[1]}
                {productCollection[2]}

            </Row>

        </Container>
    )
    
}