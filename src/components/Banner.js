import React from "react";
import { Row, Col, Container } from 'react-bootstrap'
import styles from './pages.module.css';

export const Banner = ({data}) => {
    if (data.cover === 1) {
        return (
        <section className={styles.cover1} >
            <div className={styles.mask}>
                <Container className={styles.info} >
                <Row className='h-100'>
                    <Col className='m-auto'>
                    </Col>
                        
                    <Col className='m-auto text-white'>
                        <div>
                            <h1>{data.title}</h1>
                            <p>
                                {data.description}
                            </p>
                        </div>
                    </Col>
                    <Col>
                    </Col>
                </Row>
                </Container>
                </div>
            </section>
    )
    } else if (data.cover === 2) {
        return (
            <section className={styles.cover2} >
                <Container className={styles.info} >
                <Row className='h-100'>
                    <Col className='m-auto'>
                    </Col>
                    <Col>
                    </Col>
                    <Col>
                    </Col>
                </Row>
                </Container>
            </section>
        )
    }
    

}