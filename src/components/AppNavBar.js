import { useContext } from 'react';
import { Navbar, Nav, NavDropdown} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export const AppNavBar = () => {

    const { user } = useContext(UserContext)

    return (
        <Navbar 
        variant={`${user.isAdmin ?  "light" : "dark" }`} 
        bg={`${user.isAdmin ?  "warning" : "black" }`} 
        expand="lg"
        className='fade-in' 
        sticky="top">
            <Navbar.Brand  className="text text-white" href="/">Simple T's</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
                <Link className="nav-link" to="/">Home</Link>

                <Link 
                className="nav-link" 
                to={`${user.isAdmin ?  "/products/database" : "products" }`}>
                    Products
                </Link>

                <NavDropdown title="Collection" id="basic-nav-dropdown">
                    <NavDropdown.Item>
                        <Link className="nav-link text-dark" to="/collection/arcade">Arcade</Link>
                    </NavDropdown.Item>

                    <NavDropdown.Item>
                        <Link className="nav-link text-dark" to="/collection/street">Street</Link>
                    </NavDropdown.Item>
                </NavDropdown>
            </Nav>

            {user.id !== null ?
                <Nav className='mx-4'>
                    {user.isAdmin ? <></> 
                    :
                    <>
                    <img className='my-auto mx-2' width={'25px'} height={'25px'} src={require('../assets/image/icon/whiteCart.png')} alt="logo" />
                    <img className='my-auto mx-2'  width={'25px'} height={'25px'} src={require('../assets/image/icon/whiteProfile.png')} alt="logo" />
                    </>
                    }
                    <Link className="nav-link text-white" to="/logout">Logout</Link>
                </Nav>
                :
                <Nav className='mx-4'>
                    <Link className="nav-link text-white" to="/login">Login</Link>
                    <Link className="nav-link text-white" to="/register">Register</Link>
                </Nav>
            }
                
                

            </Navbar.Collapse>
        </Navbar>
    );
};