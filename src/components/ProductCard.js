import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../pages/Home.css'
import { useContext } from 'react';
import UserContext from '../UserContext'; 


export const ProductCard = ({productProp}) => {

     const { user } = useContext(UserContext)

    if (productProp._id === '6229aa8922c20ced157286c7' ) {
        return (
            <div className='m-2'>
                <Container className='item'>
                    <img className='img-fluid mask' src={require('../assets/image/product/mockupA.jpg')} alt="logo" />
                    <Link to={`/product/view/${productProp._id}`} className="btn fLink">View Product</Link>
                    {user.isAdmin ? <Link to={`/product/update/${productProp._id}`} className="btn btn-warning fLink2">Update Product</Link> 
                        :
                        <></>
                        }
                </Container>

                <div className='mt-2'>
                    {productProp.name}
                </div>
                <div>
                    PHP: {productProp.price}
                </div> 
            </div>           
        )


    } else if (productProp._id === '6229ab1722c20ced157286c9') {
        return (
            <div className='m-2'>
                <Container className='item'>
                    <img className='img-fluid mask' src={require('../assets/image/product/mockupB.jpg')} alt="logo" />
                    <Link to={`/product/view/${productProp._id}`} className="btn fLink">View Product</Link>
                    {user.isAdmin ? <Link to={`/product/update/${productProp._id}`} className="btn btn-warning fLink2">Update Product</Link> 
                        :
                        <></>
                        }
                </Container>

                <div className='mt-2'>
                    {productProp.name}
                </div>
                <div>
                    PHP: {productProp.price}
                </div> 
            </div>           
        )
    } else if (productProp._id === '6229ab9722c20ced157286cb'){
        return (
                <div className='m-2'>
                    <Container className='item'>
                        <img className='img-fluid mask' src={require('../assets/image/product/mockupC.jpg')} alt="logo" />
                        <Link to={`/product/view/${productProp._id}`} className="btn fLink">View Product</Link>
                        {user.isAdmin ? <Link to={`/product/update/${productProp._id}`} className="btn btn-warning fLink2">Update Product</Link> 
                        :
                        <></>
                        }
                    </Container>

                    <div className='mt-2'>
                        {productProp.name}
                    </div>
                    <div>
                        PHP: {productProp.price}
                    </div> 
                </div>           
            )
    } else {
        return (
                <div className='m-2'>
                    <div className=' container-fluid item'>
                        <img className='img-fluid mask' src={productProp.thumbnail} alt="logo" />
                        <Link to={`/product/view/${productProp._id}`} className="btn fLink">View Product</Link>
                        {user.isAdmin ? <Link to={`/product/update/${productProp._id}`} className="btn btn-warning fLink2">Update Product</Link> 
                        :
                        <></>
                        }
                        
                    </div>

                    <div className='mt-2'>
                        {productProp.name}{productProp.isActive ? <></> :<span className='text-warning'>  (Archived)</span> }
                    </div>
                    <div>
                        PHP: {productProp.price}
                    </div>
                </div>           
            )
    }
    
}