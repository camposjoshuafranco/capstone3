import React from "react";
import Image from 'react-bootstrap/Image';
import { Row, Col } from 'react-bootstrap'; 
import { Link } from "react-router-dom";
import styles from './Street.module.css';



export const FeatureList = ({data}) => {

    function MouseOver(e) {
        e.target.style.borderRadius = '100px';
        e.target.style.transition = '.5s ease';
        e.target.style.filter = 'brightness(50%)';
      }

    function MouseOut(event){
            event.target.style.borderRadius = '';
            event.target.style.filter = '';
        }
    

    return (
                    <Row>
                        <Col className={styles.card} >
                            <img 
                            className="img-fluid" 
                            src={data.feature1} 
                            alt="logo"
                            onMouseOver={MouseOver} 
                            onMouseOut={MouseOut} />

                            <div className={styles.middle}>
                                <Link to={data.link1} className="btn btn-secondary">View Item</Link>
                            </div>
                            
                        </Col>
                    
                        <Col className={styles.card}>
                            <img
                            className="img-fluid" 
                            src={data.feature2} 
                            alt="logo" 
                            onMouseOver={MouseOver} onMouseOut={MouseOut}/>

                             <div className={styles.middle}>
                                <Link to={data.link2} className="btn btn-secondary">View Item</Link>
                            </div>
                        </Col>
                        <Col className={styles.card}>
                            <Image 
                            className="img-fluid" 
                            src={data.feature3} 
                            alt="logo"
                            onMouseOver={MouseOver} onMouseOut={MouseOut} />

                             <div className={styles.middle}>
                                <Link to={data.link3} className="btn btn-secondary">View Item</Link>
                            </div>
                        </Col>
                     
                </Row>           
    )
}