import React, { useState, useEffect } from "react";
import { Row, Col, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

import { ProductCard } from "../components/ProductCard";


export const ProductAll = () => {

    const [productCollection, setProductCollection] = useState([])

    useEffect(() => {

        let token = localStorage.getItem('accessToken')

        fetch('https://young-plateau-70373.herokuapp.com/products/all-items', {
            headers: {
               Authorization: `Bearer ${token}` 
            }
        })
        .then(res => res.json())
        .then(convertedData => {
            setProductCollection(convertedData.map(product => {
            return (
                <Col xs={12} md={6} lg={4} key={product._id}>
                    <ProductCard productProp={product} />
                </Col>
            )
        }))
        })

    }, [setProductCollection]);

    return (
        <Container className="text-center">
            <h3 className='text font-weight-bold mt-4'>PRODUCTS DATABASE</h3>
            <Row>
                <Col>
                <Link className="btn btn-rounded btn-danger" to='/products/create'>Add Product</Link>
                </Col>
            </Row>
            <Row>
                {productCollection}
            </Row>

        </Container>
    )
    
}