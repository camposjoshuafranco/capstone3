import { Link } from 'react-router-dom';
import { useInView } from 'react-intersection-observer';

import './Home.css'
import { Catalog } from '../components/Catalog';


export const Home = () => {

        const { ref: myRef, inView: isSectVisible } = useInView();
        const { ref: myRef2, inView: isLanding } = useInView();

    

    return (
        <>
        
        <section ref={myRef2} className={`landing ${isLanding ? 'fade-in' : ''}`}>
        </section>
        
        <section className='collection text-center p-5'>
                <h3 className='text font-weight-bold m-3'>COLLECTION</h3>
                <Link to='/collection/arcade'>
                <div className='fluid feature1 text-center' >
                    <h3 className='text-white text fLink m-auto'>VIEW ARCADE STYLE</h3>
                </div>
                </Link>

                <Link to='/collection/street'>
                <div className='fluid feature2'>
                    <h3 className='text-white text fLink m-auto'>VIEW STREET STYLE</h3>
                </div>
                </Link>

                <Link to='/products'>
                <div className='fluid feature3'>
                    <h3 className='text-white text fLink m-auto'>VIEW ALL PRODUCTS</h3>
                </div>
                </Link>
        </section>
        <section ref={myRef} className={`mt-5 mb-5 pt-5 ${isSectVisible ? 'fade-in slide-in' : ''}`}>
            <Catalog/>
        </section>
        

        </>
        
    )
}