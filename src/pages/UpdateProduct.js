import { useState, useEffect } from 'react'; 
import { useParams } from 'react-router-dom'; 
import { Container, Form, Button, InputGroup, FormControl, Row,Col } from 'react-bootstrap';

export const UpdateProduct = () => {

    const {id} = useParams();

    const [productInfo, setProductInfo] = useState({
		name: null,
		description: null,
		price: null,
        thumbnail: null
	});

    useEffect(() => {
    	fetch(`https://young-plateau-70373.herokuapp.com/products/${id}`)
        .then(res => res.json())
    	.then(convertedData => {
    		setProductInfo({
    			name:  convertedData.name,
    			description: convertedData.description,
    			price: convertedData.price,
                thumbnail: convertedData.thumbnail

    		}) 
    	}); 

    },[id, setProductInfo])
    

    const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
    const [thumbnail, setThumbnail] = useState('');
	const [price, setPrice] = useState('');
    const [isActive, setIsActive] = useState(true);

    const [isFailed, setIsFailed] = useState('');

    const addProduct = async (eventSubmit) => {
        eventSubmit.preventDefault();

         let token = localStorage.getItem('accessToken')

        const isRegistered = await fetch(`https://young-plateau-70373.herokuapp.com/products/${id}/update`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
            name: productName,
            description: description,
            price: price,
            thumbnail: thumbnail,
            isActive: isActive
        })
        }).then(response => response.json()).then(dataJSON => {
            if (dataJSON) {
                return true
            } else {
                return false
            }
        } )

        if (isRegistered) {
            
            setProductName('');
            setDescription('');
            setPrice('');
            setThumbnail('');
            setIsActive(true);
            
            window.location.href = "/products/database"; 

        } else {
            setIsFailed(true) 
            setIsActive(true);
        }   
    }

    const onSwitchAction = () => {
        setIsActive(!isActive);
    };

    return (
        <>
        {isFailed ? <h3 className='text-center text-danger'>Failed to Add</h3> : <></>}
        <div className='p-5 w-75 container-fluid'>
                    <Row className=''>
                        <Col className='fluid'>
                            <Container className='text-center'>
                               <img className='img-fluid' src={productInfo.thumbnail} alt="logo" /> 
                            </Container>
                            
                        </Col>
                        <Col>
                            <Container className=''>
                                <Form onSubmit={ e => addProduct(e)}>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Product Name :</Form.Label>
                                        <Form.Control type="text" 
                                        placeholder={productInfo.name}
                                        required
                                        value={productName}
                                        onChange={e => setProductName(e.target.value)} />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Description :</Form.Label>
                                        <Form.Control as="textarea" 
                                        rows={3}
                                        placeholder={productInfo.description}
                                        required
                                        value={description}
                                        onChange = {e => setDescription(e.target.value)} />
                                    </Form.Group>

                                    <Form.Group className="mb-3">
                                            <Form.Label>Price :</Form.Label>
                                            <InputGroup>
                                                <InputGroup.Text>PHP</InputGroup.Text>
                                                <Form.Control 
                                                    type="number"
                                                    placeholder={productInfo.price}
                                                    required
                                                    value={price}
                                                onChange={e => setPrice(e.target.value)}
                                                />
                                            </InputGroup>
                                        </Form.Group>
                                        
                                    <Form.Group className="mb-3">
                                        <Form.Label>Product Thumbnail :</Form.Label>
                                        <InputGroup >
                                            <InputGroup.Text>URL</InputGroup.Text>
                                            <FormControl 
                                                type="text" 
                                                placeholder={productInfo.thumbnail}
                                                required
                                                value={thumbnail}
                                                onChange={e => setThumbnail(e.target.value)}  />
                                        </InputGroup>
                                    </Form.Group>
                                    <Form.Group className='ml-4 mb-3'>
                                        
                                        <Form.Switch
                                            onChange={onSwitchAction}
                                            id="custom-switch"
                                            label={isActive ? "Product Active" : "Product Archived"}
                                            checked={isActive}
                                        />
                                    </Form.Group>
                                    {isActive ? <></> : <Form.Label className='rounded p-1 bg-secondary'>Archived!</Form.Label>   }
                                    
                                        <Button 
                                            className="btn btn-warning btn-block my-4"
                                            type="submit"
                                        >Update Product</Button>
                                </Form>    
                            </Container>
                        </Col>
                    </Row>
        </div>
        </>
    )
}