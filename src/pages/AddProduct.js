import { useState } from 'react'; 
import { Container, Form, Button, InputGroup, FormControl, Row, Col } from 'react-bootstrap';

export const AddProduct = () => {

    const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
    const [thumbnail, setThumbnail] = useState('');
	const [price, setPrice] = useState('');

    const [isFailed, setIsFailed] = useState('');

    const addProduct = async (eventSubmit) => {
        eventSubmit.preventDefault();

         let token = localStorage.getItem('accessToken')

        const isRegistered = await fetch('https://young-plateau-70373.herokuapp.com/products/create', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
            name: productName,
            description: description,
            price: price,
            thumbnail: thumbnail
        })
        }).then(response => response.json()).then(dataJSON => {
            if (dataJSON) {
                return true
            } else {
                return false
            }
        } )

        if (isRegistered) {
            
            setProductName('');
            setDescription('');
            setPrice('');
            setThumbnail('');
            
            window.location.href = "/products/database"; 

        } else {
            setIsFailed(true) 
        }   
    }

    return (
        <>
        {isFailed ? <h3 className='text-center text-danger'>Failed to Add</h3> : <></>}
        <div className='p-5 w-75 container-fluid'>
            <Row>
                <Col>
                    <Container className='text-center'>
                        <img className='img-fluid rounded' src="https://images.unsplash.com/photo-1625698311031-f0dd15be5144?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80" alt="logo" /> 
                    </Container>
                </Col>
                <Col>
                    <Container className='mx-auto my-5'>
                        <div className='text-center'>
                            <h2 className='text'>Simple T's</h2>
                        </div>
                        <Form onSubmit={ e => addProduct(e)}>
                            <Form.Group className="mb-3">
                                <Form.Label>Product Name :</Form.Label>
                                <Form.Control type="text" 
                                placeholder="Abc"
                                required
                                value={productName}
                                onChange={e => setProductName(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                <Form.Label>Description :</Form.Label>
                                <Form.Control as="textarea" 
                                rows={3}
                                required
                                value={description}
                                onChange = {e => setDescription(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3">
                                    <Form.Label>Price :</Form.Label>
                                    <InputGroup>
                                        <InputGroup.Text>PHP</InputGroup.Text>
                                        <Form.Control 
                                            type="number"
                                            placeholder="Enter Price"
                                            required
                                            value={price}
                                        onChange={e => setPrice(e.target.value)}
                                        />
                                    </InputGroup>
                                </Form.Group>
                                
                            <Form.Group className="mb-3">
                                <Form.Label>Product Thumbnail :</Form.Label>
                                <InputGroup >
                                    <InputGroup.Text>URL</InputGroup.Text>
                                    <FormControl 
                                        type="text" 
                                        placeholder="https://"
                                        required
                                        value={thumbnail}
                                        onChange={e => setThumbnail(e.target.value)}  />
                                </InputGroup>
                            </Form.Group>

                                <Button 
                                    className="btn btn-red btn-block my-4"
                                    type="submit"
                                >Add Product</Button>
                        </Form>    
                    </Container>
                </Col>
            </Row>
        </div>
        
        </>
    )
}