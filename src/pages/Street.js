import React from "react";
import { Banner } from '../components/Banner';
import { FeatureList } from "../components/FeatureList";


const content = {
    cover: 2,
    title: 'Ready, Set. Game!',
    description: 'Arcade style is unique fun and fashionable. Arcade style is for every type of person. You will be hard pressed to find a company who has clothing as fun and as unique as we do. We put a little bit of joy in every purchase. It is this fun and creativity that makes our clothes so special.',
    feature1: require('../assets/image/street/modelA3a.jpg'),
    feature2: require('../assets/image/street/modelA2.jpg'),
    feature3: require('../assets/image/street/modelA3.jpg'),
    link1: '/product/view/622a8de4ddb97efa1b3ab517',
    link2:  '/product/view/622a8e7cddb97efa1b3ab654',
    link3:  '/product/view/622a8e24ddb97efa1b3ab59b'
}


export const Street = () => {
    return (
        <div>
            <Banner data={content}/>
            <section className='bg-white p-5'>
                <h1  className="text-center text-dark text mb-5">Street Style</h1>
                <FeatureList data={content} />
            </section>
        </div>
        
    )
}