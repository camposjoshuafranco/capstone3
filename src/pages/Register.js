import { useState, useEffect, useContext } from 'react'; 

import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext'; 

export const Register = () => {

    const { user } = useContext(UserContext)

    const [firstName,setFirstName] = useState('');
    const [lastName,setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(false);
    const [isMatched, setIsMatched] = useState(false);
    const [isValid, setIsValid] = useState(false);
    const [isAllowed, setIsAllowed] = useState(false);
    const [isFailed, setIsFailed] = useState(false);

    useEffect(() => { 
        
        if ( mobile.length === 11){
            setIsValid(true);
            
            if (password1 === password2 && password1 !== '' && password2 !== '') {
                setIsMatched(true);
                if (firstName !== '' && lastName !== '' && email !== ''){
                    setIsActive(true);
                    setIsAllowed(true);
                } else {
                    setIsActive(false);
                    setIsAllowed(false);
                }

            }else {
                setIsMatched(false);
                setIsActive(false);
                setIsAllowed(false);
            }
		} else if (password1 !== '' && password1 === password2){
            setIsMatched(true)
        } else {
            setIsValid(false);
            setIsMatched(false);
			setIsActive(false);
		};

    }, [firstName, lastName, email, mobile, password1,password2] )

    const registerUser = async (eventSubmit) => {
    eventSubmit.preventDefault();


    const isRegistered = await fetch('https://young-plateau-70373.herokuapp.com/users/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            mobile: mobile,
            password: password1,
        })
        }).then(response => response.json()).then(dataJSON => {
            if (dataJSON.email) {
                return true
            } else {
                return false
            }
        } )

        if (isRegistered) {
            
            setEmail('');
            setFirstName('');
            setLastName('');
            setMobile('');
            setPassword1('');
            setPassword2('');
            
            window.location.href = "/login"; 

        } else {
            setIsFailed(true) 
            setIsAllowed(false);
        }   
    };


    return (
        user.id ?
            <Navigate to="/" replace={true} />
        :
        <>
            <Container className="mb-5 pb-5 w-50">

                <h1 className="text-center">Registration Form</h1>
                {isFailed ? 
                <>
                <h3 className="text-center mt-3 text-danger">Registration Failed! </h3>
                <h5 className="text-center mt-3 text-danger">Email Already Exist</h5>
                </> 
                : 
                <></>}

                {isAllowed ?
                <></>
                :
                <h6 className="text-center mt-3 text-secondary">Please fill out form</h6>}
                
                <Form className="mb-5" onSubmit={ e => registerUser(e)}>
                    <Form.Group>
                        <Form.Label>First Name</Form.Label>
                        <Form.Control 
                            type="text"
                            placeholder="Enter First Name"
                            required
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
                            type="text"
                            placeholder="Enter Last Name"
                            required
                            value={lastName}
                            onChange={e => setLastName(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control 
                            type="email"
                            placeholder="Enter Email"
                            required
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Mobile Number:</Form.Label>
                        <Form.Control 
                            type="number"
                            placeholder="Insert your Mobile No. [11 digits]"
                            required
                            value={mobile}
                            onChange={e => setMobile(e.target.value)}
                        />
                        {isValid ?
                        <span className="text-success"> Mobile number Valid!</span>
                        :
                            <span className="text-danger"> Mobile number not valid!</span>
                        }
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Password:</Form.Label>
                        <Form.Control 
                            type="password"
                            placeholder="Enter your Password"
                            required
                            value={password1}
                            onChange={e => setPassword1(e.target.value)}
                        />
                    </Form.Group>

                    {/* confirm password */}
                    <Form.Group className='mb-5'>
                        <Form.Label>Confirm Password:</Form.Label>
                        <Form.Control 
                            type="password"
                            placeholder="Confirm your Password"
                            required
                            value={password2}
                            onChange={e => setPassword2(e.target.value)}
                        />
                        {
                            isMatched ? 
                            <span className="text-success">
                            Passwords Matched!
                            </span>
                            :
                            <span className="text-danger">
                            Passwords Should Match!
                            </span>
                        }
                        
                        
                    </Form.Group>

                    {
                        isActive ? <Button 
                        className="btn-black btn-block"
                        type="submit"
                    >
                        Register
                    </Button> : <Button 
                        className="btn-white btn-block"
                        
                    >
                        Register
                    </Button>
                    }

                </Form>
                
            </Container>
        </>
    );
}
