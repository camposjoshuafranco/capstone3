import { useState, useEffect } from 'react';
import  { Row, Col, Form, Button } from 'react-bootstrap';

import { useParams } from 'react-router-dom'; 
import { ProductDetail } from '../components/ProductDetail';

import './Home.css'

export const ProductView = () => {

	const [productInfo, setProductInfo] = useState({
		name: null,
		description: null,
		price: null,
        thumbnail: null
	}); 

	const {id} = useParams() 

    useEffect(() => {
    	fetch(`https://young-plateau-70373.herokuapp.com/products/${id}`)
        .then(res => res.json())
    	.then(convertedData => {
    		setProductInfo({
    			name:  convertedData.name,
    			description: convertedData.description,
    			price: convertedData.price,
                thumbnail: convertedData.thumbnail

    		}) 
    	}); 
    },[id])


        if (id === '6229aa8922c20ced157286c7' ) {
            const content = {
                img1: require('../assets/image/product/mockupA.jpg'),
                img2: require('../assets/image/product/mockupA2.jpg'),
                img3: require('../assets/image/product/mockupA3.jpg')
            }

            return(
                <>
                <div className='p-5 container-fluid'>
                    <div className="text-center mb-5">
                        <h1 className='text'>Simple T's</h1>
                    </div>
                    <Row className=''>
                        <Col lg={7} className=''>
                            <ProductDetail data={content}/>
                        </Col>
                    <Col className='' >
                        <h2 className='mb-4'><b>{productInfo.name}</b></h2>
                            <Form>
                                <Form.Label>Sizes</Form.Label>
                                <Form.Select className='w-25 mb-2' aria-label="Default select example">
                                    <option value="sm">XS</option>
                                    <option value="sm">SM</option>
                                    <option value="md">MD</option>
                                    <option value="lg">LG</option>
                                    <option value="xl">XL</option>
                                    <option value="xxl">XXL</option>
                                    <option value="xxxl">XXXL</option>
                                </Form.Select>

                                <Form.Label>QTY</Form.Label>
                                <Form.Select className='w-25 mb-4' aria-label="Default select example">
                                    <option value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </Form.Select>

                                <Button className="btn-black mb-5" type="submit">
                                    Add to Cart
                                </Button>
                            </Form>
                            
                            <h5 className='mb-4'>PHP {productInfo.price}</h5>
                            <h6 className='mb-4'>{productInfo.description}</h6>

                            <h6>Made of 100% combed cotton</h6>
                            <ul>
                                <li>Machine wash cold</li>
                                <li>Dry cleanable</li>
                                <li>Do not iron on print</li>
                                <li>Do not bleach</li>
                                <li>Do not wring</li>
                                <li>Do not tumble dry</li>
                            </ul>
                        </Col>
                    </Row>
                </div>
                </>
	        ); 
        } else if (id === '6229ab1722c20ced157286c9') {
            let content = {
                img1: require('../assets/image/product/mockupB.jpg'),
                img2: require('../assets/image/product/mockupB1.jpg'),
                img3: require('../assets/image/product/mockupB2.jpg')
            }

            return(
            <>
            <div className='p-5 container-fluid'>
                <div className="text-center mb-5">
                        <h1 className='text'>Simple T's</h1>
                    </div>
                <Row className=''>
                    <Col lg={7} className=''>
                        <ProductDetail data={content} />
                    </Col>
                    <Col className='' >
                        <h2 className='mb-4'><b>{productInfo.name}</b></h2>
                            <Form>
                                <Form.Label>Sizes</Form.Label>
                                <Form.Select className='w-25 mb-2' aria-label="Default select example">
                                    <option value="sm">XS</option>
                                    <option value="sm">SM</option>
                                    <option value="md">MD</option>
                                    <option value="lg">LG</option>
                                    <option value="xl">XL</option>
                                    <option value="xxl">XXL</option>
                                    <option value="xxxl">XXXL</option>
                                </Form.Select>

                                <Form.Label>QTY</Form.Label>
                                <Form.Select className='w-25 mb-4' aria-label="Default select example">
                                    <option value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </Form.Select>

                                <Button className="btn-black mb-5" type="submit">
                                    Add to Cart
                                </Button>
                            </Form>
                            
                            <h5 className='mb-4'>PHP {productInfo.price}</h5>
                            <h6 className='mb-4'>{productInfo.description}</h6>

                            <h6>Made of 100% combed cotton</h6>
                            <ul>
                                <li>Machine wash cold</li>
                                <li>Dry cleanable</li>
                                <li>Do not iron on print</li>
                                <li>Do not bleach</li>
                                <li>Do not wring</li>
                                <li>Do not tumble dry</li>
                            </ul>
                    </Col>
                </Row>
            </div>
            </>
            );
        } else if (id === '6229ab9722c20ced157286cb') {
            let content = {
                img1: require('../assets/image/product/mockupC.jpg'),
                img2: require('../assets/image/product/mockupC1.jpg'),
                img3: require('../assets/image/product/mockupC2.jpg')
            }

            return(
            <>
            <div className='p-5 w-75 container-fluid'>
                <div className="text-center mb-5">
                        <h1 className='text'>Simple T's</h1>
                    </div>
                <Row className=''>
                    <Col className=''>
                        <ProductDetail data={content} />
                    </Col>
                    <Col className='' >
                        <h2 className='mb-4'><b>{productInfo.name}</b></h2>
                            <Form>
                                <Form.Label>Sizes</Form.Label>
                                <Form.Select className='w-25 mb-2' aria-label="Default select example">
                                    <option value="sm">XS</option>
                                    <option value="sm">SM</option>
                                    <option value="md">MD</option>
                                    <option value="lg">LG</option>
                                    <option value="xl">XL</option>
                                    <option value="xxl">XXL</option>
                                    <option value="xxxl">XXXL</option>
                                </Form.Select>

                                <Form.Label>QTY</Form.Label>
                                <Form.Select className='w-25 mb-4' aria-label="Default select example">
                                    <option value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </Form.Select>

                                <Button className="btn-black mb-5" type="submit">
                                    Add to Cart
                                </Button>
                            </Form>
                            
                            <h5 className='mb-4'>PHP {productInfo.price}</h5>
                            <h6 className='mb-4'>{productInfo.description}</h6>

                            <h6>Made of 100% combed cotton</h6>
                            <ul>
                                <li>Machine wash cold</li>
                                <li>Dry cleanable</li>
                                <li>Do not iron on print</li>
                                <li>Do not bleach</li>
                                <li>Do not wring</li>
                                <li>Do not tumble dry</li>
                            </ul>
                    </Col>
                </Row>
            </div>
            </>
            );
        } else {

            return (
                <>
                <div className='p-5 w-75 container-fluid'>
                    <div className="text-center mb-5">
                        <h1 className='text'>Simple T's</h1>
                    </div>
                    
                    <Row className=''>
                        <Col className=''>
                            <img className='img-fluid mask' src={productInfo.thumbnail} alt="logo" />
                        </Col>
                        <Col className=''>
                            <h2 className='mb-4'><b>{productInfo.name}</b></h2>
                            <Form>
                                <Form.Label>Sizes</Form.Label>
                                <Form.Select className='w-25 mb-2' aria-label="Default select example">
                                    <option value="sm">XS</option>
                                    <option value="sm">SM</option>
                                    <option value="md">MD</option>
                                    <option value="lg">LG</option>
                                    <option value="xl">XL</option>
                                    <option value="xxl">XXL</option>
                                    <option value="xxxl">XXXL</option>
                                </Form.Select>

                                <Form.Label>QTY</Form.Label>
                                <Form.Select className='w-25 mb-4' aria-label="Default select example">
                                    <option value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </Form.Select>

                                <Button className="btn-black mb-5" type="submit">
                                    Add to Cart
                                </Button>
                            </Form>
                            
                            <h5 className='mb-4'>PHP {productInfo.price}</h5>
                            <h6 className='mb-4'>{productInfo.description}</h6>

                            <h6>Made of 100% combed cotton</h6>
                            <ul>
                                <li>Machine wash cold</li>
                                <li>Dry cleanable</li>
                                <li>Do not iron on print</li>
                                <li>Do not bleach</li>
                                <li>Do not wring</li>
                                <li>Do not tumble dry</li>
                            </ul>
                        </Col>
                    </Row>
                </div>
                </>
            )

        }

        
};
