import React, { useState, useEffect, useContext } from "react";
import { Row, Col, Container } from "react-bootstrap";
import UserContext from "../UserContext";

import { ProductCard } from "../components/ProductCard";


export const ProductActive = () => {
    const [productCollection, setProductCollection] = useState([])

    const { user } = useContext(UserContext);

    useEffect(() => {
        fetch('https://young-plateau-70373.herokuapp.com/products/active')
        .then(res => res.json())
        .then(convertedData => {
            setProductCollection(convertedData.map(product => {
            return (
                <Col xs={12} md={6} lg={4} key={product._id}>
                    <ProductCard productProp={product} />
                </Col>
            )
        }))
        })

    }, [setProductCollection]);

    return (
        <Container className="text-center">
            {user.isAdmin ? <h4 className='text-warning'>Viewing as User</h4> : <></>}
            <h3 className='text font-weight-bold my-5'>PRODUCTS</h3>
            
            <Row>
                {productCollection}
            </Row>

        </Container>
    )
    
}