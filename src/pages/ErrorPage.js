import { Container, Spinner } from "react-bootstrap"

export const ErrorPage = () => {
    return(
        <Container className="p-5 text-center">
            
                <Spinner  animation="border" />
                <h4>If this takes forever page not exist</h4>
                <h5>or you are not authorized</h5>
                <h6>Check your connection</h6>
            
        </Container>
    )
}