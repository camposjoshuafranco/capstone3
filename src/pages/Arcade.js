import { Banner } from '../components/Banner';
import { FeatureList } from '../components/FeatureList';

const content = {
    cover: 1,
    title: 'Ready, Set. Game!',
    description: 'Arcade style is unique fun and fashionable. The design is for every type of person.',
    feature1: require('../assets/image/arcade/side1.jpg'),
    feature2: require('../assets/image/arcade/modelA2.jpg'),
    feature3: require('../assets/image/arcade/modelB1.jpg'),
    link1: '/product/view/622a82ea6777651e3b469c69',
    link2:  '/product/view/622a8c3cddb97efa1b3ab186',
    link3:  '/product/view/622a8ce7ddb97efa1b3ab2f9'
}

export const Arcade = () => {
    return (
        <div>
            <Banner data={content}/>
            <section className='black p-5'>
                <h1 className="text-center text-white mb-5">Arcade Style</h1>
                
                <FeatureList data={content}/> 
            </section>
        </div>
        
    )
}
