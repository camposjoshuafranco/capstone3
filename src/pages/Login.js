import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext'; 
import { Navigate } from 'react-router-dom'; 

import {Form, Button, Container} from 'react-bootstrap';


export const Login = () => {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail ] = useState('');	
    const [password, setPassword ] = useState('');

    let addressSign = email.search('@');
    let dns = email.search('.com');

    const [isActive, setIsActive] = useState(false);
    const [isValid, setIsValid] = useState(false);
    const [isFailed, setIsFailed] = useState(false)

    useEffect(() => {
    	if (dns !== -1 && addressSign !== -1 ) {
    		setIsValid(true);
    		if (password !== '') {
    			setIsActive(true);
    		} else {
    			setIsActive(false);
    		}
    	} else {
    		setIsValid(false); 
    		setIsActive(false); 
    	}
    },[email, password, addressSign, dns])


    const loginUser = async (event) => {
		event.preventDefault(); 
		 
		fetch('https://young-plateau-70373.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => res.json())
		.then(dataNaJson => {
			 
			let token = dataNaJson.accessToken; 
			

			 
			if (typeof token !== 'undefined') {
                
				localStorage.setItem('accessToken',token);

				fetch('https://young-plateau-70373.herokuapp.com/users/profile', {
				   headers: {
				      Authorization: `Bearer ${token}`
				   }
				})
				.then(res => res.json())
				.then(convertedData => {
				   
				   if (typeof convertedData._id !== "undefined") {

				     setUser({
				        id: convertedData._id, 
				        isAdmin: convertedData.isAdmin
				     });

				 	 window.location.href = "/"; 

				   } else {
				      setUser({
				        id: null,
				        isAdmin: null
				     });

                     setIsFailed(true);
				   }
				}); 
			} else {
				setIsFailed(true);
			}
		})

		
	};


    return (
        user.id 
        ?
        <Navigate to="/" replace={true}/> 
        :
        <>
        <Container className="my-5 w-50 border rounded">
            <h1 className="text-center">Login</h1>
            {isFailed ? <h2 className="text-center text-danger">Login Failed. Check credentials</h2> : <></>}
            <Form className="mb-5 my-5" onSubmit={ e => loginUser(e)}>
                <Form.Group>
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter Email"
                        required
                        value={email}
                        onChange={event => {setEmail(event.target.value)} }
                    />
                    {isValid ? <h6 className="text-success">Email is Valid!</h6> : <h6 className="text-secondary">Email is not Valid</h6>}
                </Form.Group>
                <Form.Group className='my-4'>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Enter Paassword"
                        required
                        value={password}
                        onChange={e => {setPassword(e.target.value)}}
                    />
                </Form.Group>
                {
                    isActive ?
                    <Button 
                        className="btn-info btn-main btn-block"
                        type="submit"
                    >
                        Log in
                    </Button>
                        :
                        <Button 
                        className="btn-secondary btn-block"
                        disabled
                    >
                        Log in
                    </Button>
                }
            </Form>
        </Container>
        </>
    )
}